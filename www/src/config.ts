export interface Config<Params extends Record<string, any>> {
  get: (key: keyof Params & string) => Params[keyof Params & string];
  set: (
    key: keyof Params & string,
    value: Params[keyof Params & string]
  ) => void;
}

export const config = <Params extends Record<string, any>>(
  initialValues: Params
): Config<Params> => {
  type Key = keyof Params & string;
  const values = initialValues;

  const get = (key: Key) => values[key];
  const set = (key: Key, value: Params[Key]) => {
    values[key] = value;
  };

  return { get, set };
};
