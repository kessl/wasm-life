import { Universe } from "wasm-life";

let GRID_COLOR: string, DEAD_COLOR: string, ALIVE_COLOR: string;

const applyLightTheme = () => {
  GRID_COLOR = "#ccc";
  DEAD_COLOR = "#fff";
  ALIVE_COLOR = "#000";
};

const applyDarkTheme = () => {
  GRID_COLOR = "#000";
  DEAD_COLOR = "#000";
  ALIVE_COLOR = "#fff";
};

if (window.matchMedia("(prefers-color-scheme: dark)").matches) {
  applyDarkTheme();
}

window
  .matchMedia("(prefers-color-scheme: dark)")
  .addEventListener("change", (e) => {
    e.matches ? applyDarkTheme() : applyLightTheme();
  });

const getIndex = (universe: Universe, row: number, column: number) => {
  return row * universe.width() + column;
};

const bitIsSet = (n: number, arr: Uint8Array) => {
  const byte = Math.floor(n / 8);
  const mask = 1 << n % 8;
  return (arr[byte] & mask) === mask;
};

export const drawGrid = (
  canvas: HTMLCanvasElement,
  universe: Universe,
  CELL_SIZE: number
) => {
  const ctx = canvas.getContext("2d");
  if (!ctx) {
    throw new Error("Could not acquire canvas context.");
  }

  ctx.beginPath();
  ctx.strokeStyle = GRID_COLOR;

  // Vertical lines.
  for (let i = 0; i <= universe.width(); i++) {
    ctx.moveTo(i * (CELL_SIZE + 1) + 1, 0);
    ctx.lineTo(
      i * (CELL_SIZE + 1) + 1,
      (CELL_SIZE + 1) * universe.height() + 1
    );
  }

  // Horizontal lines.
  for (let j = 0; j <= universe.height(); j++) {
    ctx.moveTo(0, j * (CELL_SIZE + 1) + 1);
    ctx.lineTo((CELL_SIZE + 1) * universe.width() + 1, j * (CELL_SIZE + 1) + 1);
  }

  ctx.stroke();
};

export const drawCells = (
  canvas: HTMLCanvasElement,
  universe: Universe,
  memory: WebAssembly.Memory,
  CELL_SIZE: number
) => {
  const ctx = canvas.getContext("2d");
  if (!ctx) {
    throw new Error("Could not acquire canvas context.");
  }

  const cells = new Uint8Array(
    memory.buffer,
    universe.cells(),
    (universe.width() * universe.height()) / 8
  );

  ctx.beginPath();

  for (let row = 0; row < universe.height(); row++) {
    for (let col = 0; col < universe.width(); col++) {
      const idx = getIndex(universe, row, col);

      ctx.fillStyle = bitIsSet(idx, cells) ? ALIVE_COLOR : DEAD_COLOR;

      ctx.fillRect(
        col * (CELL_SIZE + 1) + 1,
        row * (CELL_SIZE + 1) + 1,
        CELL_SIZE,
        CELL_SIZE
      );
    }
  }

  ctx.stroke();
};
