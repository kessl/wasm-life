import { initialConfig } from "..";
import { Config } from "./config";
import { Universe } from "wasm-life";
import { LoopControls } from "./loop";

interface EventContext {
  canvas: HTMLCanvasElement;
  universe: Universe;
  conf: Config<typeof initialConfig>;
  render: () => void;
  controls: LoopControls;
}

type EventRegisterFn = (context: EventContext) => void;

export const registerEvents = (context: EventContext) => {
  registerResizeEvents(context);
  registerClickEvents(context);
  registerSpeedControlEvents(context);
  registerGameControlEvents(context);
};

const registerResizeEvents: EventRegisterFn = ({ canvas, universe, conf }) => {
  const ctx = canvas.getContext("2d");
  if (!ctx) return;

  const resizeCanvas = () => {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    conf.set(
      "CELL_SIZE",
      (document.documentElement.clientHeight - 300) / universe.height()
    );
    canvas.height = (conf.get("CELL_SIZE") + 1) * universe.height() + 1;
    canvas.width = (conf.get("CELL_SIZE") + 1) * universe.width() + 1;
  };

  resizeCanvas();
  window.addEventListener("resize", resizeCanvas);
};

const registerClickEvents: EventRegisterFn = ({
  canvas,
  universe,
  conf,
  render,
}) => {
  const handleCanvasClick = (e: MouseEvent) => {
    const boundingRect = canvas.getBoundingClientRect();

    const canvasLeft =
      ((e.clientX - boundingRect.left) * canvas.width) / boundingRect.width;
    const canvasTop =
      ((e.clientY - boundingRect.top) * canvas.height) / boundingRect.height;

    const row = Math.min(
      Math.floor(canvasTop / (conf.get("CELL_SIZE") + 1)),
      universe.height() - 1
    );
    const col = Math.min(
      Math.floor(canvasLeft / (conf.get("CELL_SIZE") + 1)),
      universe.width() - 1
    );

    universe.toggle_cell(row, col);
    render();
  };

  canvas.addEventListener("click", handleCanvasClick);
};

const registerSpeedControlEvents: EventRegisterFn = ({ conf }) => {
  const slider = document.getElementById("speed") as HTMLInputElement;
  const speedIndicatorDiv = document.getElementById("speed-indicator");
  slider.value = "1";

  const updateSpeed = (spd: number) => {
    conf.set("speed", spd);
    if (speedIndicatorDiv) {
      speedIndicatorDiv.innerHTML = `${spd} ticks per frame`;
    }
  };

  updateSpeed(+slider.value);
  slider.addEventListener("change", (e) =>
    updateSpeed(+(e.target as HTMLInputElement)?.value)
  );
};

const registerGameControlEvents: EventRegisterFn = ({
  universe,
  render,
  controls,
}) => {
  document.getElementById("reset")?.addEventListener("click", () => {
    universe.reset();
    render();
  });

  document.getElementById("randomize")?.addEventListener("click", () => {
    universe.randomize();
    render();
  });

  const playPauseButton = document.getElementById("play-pause");
  if (playPauseButton) {
    playPauseButton.textContent = "⏸";
    playPauseButton?.addEventListener("click", () => {
      if (controls.isPaused()) {
        playPauseButton.textContent = "⏸";
        controls.play();
      } else {
        playPauseButton.textContent = "▶";
        controls.pause();
      }
    });
  }
};
