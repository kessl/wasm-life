export interface LoopControls {
  play: () => void;
  pause: () => void;
  isPaused: () => boolean;
}

export const loop = (update: (dt: number) => void): LoopControls => {
  let nextFrameId: number | undefined = undefined;

  const isPaused = () => nextFrameId === undefined;

  const play = () => {
    gameLoop();
  };

  const pause = () => {
    if (nextFrameId) {
      cancelAnimationFrame(nextFrameId);
      nextFrameId = undefined;
    }
  };

  let lastFrameTime = window.performance.now();
  const gameLoop = () => {
    nextFrameId = requestAnimationFrame(gameLoop);

    const now = window.performance.now();
    const dt = now - lastFrameTime;
    lastFrameTime = now;

    update(dt);
  };

  return { play, pause, isPaused };
};
