import "./default.css";
import { config } from "./src/config";
import { Universe } from "wasm-life";
import { memory } from "wasm-life/wasm_life_bg";
import { loop } from "./src/loop";
import { drawGrid, drawCells } from "./src/render";
import { registerEvents } from "./src/gui";

const canvas = document.getElementById("life-canvas") as HTMLCanvasElement;

const universe = Universe.new();

export const initialConfig = {
  speed: 1,
  CELL_SIZE: (document.documentElement.clientHeight - 300) / universe.height(),
};

const conf = config(initialConfig);

const render = () => {
  drawGrid(canvas, universe, conf.get("CELL_SIZE"));
  drawCells(canvas, universe, memory, conf.get("CELL_SIZE"));
};

const controls = loop((dt) => {
  for (let i = 0; i < conf.get("speed"); ++i) {
    universe.tick();
  }
  render();
});

registerEvents({ canvas, universe, conf, render, controls })
controls.play();
